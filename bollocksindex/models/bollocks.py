import pymongo
from bson.objectid import ObjectId
import markdown2

client = pymongo.MongoClient()
db = client.bollocksindex
collection = db.bollocks

markdowner = markdown2.Markdown()


class Bollocks(object):
    def __init__(self, he_said, it_means, _id=None):
        self.data = {"he_said": he_said, "it_means": it_means}
        self.he_said = markdowner.convert(he_said)
        self.it_means = markdowner.convert(it_means)
        self._id = _id
        if _id is not None:
            self._id = ObjectId(_id)

    def push(self):
        return collection.insert(self.data)

    @classmethod
    def get_by_id(cls, _id):
        i = ObjectId(_id)
        data = collection.find_one({'_id':i})
        try:
            return cls(data['he_said'], data['it_means'], data['_id'])
        except KeyError:
            return cls(0, data['he_said'], data['it_means'], data['_id'])

    @classmethod
    def get(cls, **kwargs):
        data = collection.find_one(kwargs)
        return cls(data['he_said'], data['it_means'], data['_id'])

    @classmethod
    def get_all(cls, **kwargs):
        d = {}
        for data in collection.find(kwargs):
            try:
                yield cls(data['he_said'], data['it_means'], data['_id'])
            except KeyError:
                yield cls(0, data['he_said'], data['it_means'], data['_id'])

    @staticmethod
    def remove(he_said=None, it_means=None):
        d = {}
        if he_said is not None:
            d["he_said"] = he_said

        if it_means is not None:
            d["it_means"] = it_means

        document = collection.find_one(d)
        return collection.remove(document["_id"])


__all__ = ["Bollocks"]
