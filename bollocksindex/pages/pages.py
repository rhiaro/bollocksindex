from webapp2 import uri_for

from bollocksindex.pages import BaseHandler
from bollocksindex.application import application as app

from bollocksindex.utils import redirect, clean_html

from bollocksindex.models.bollocks import Bollocks


@app.route('/', 'index')
class Index(BaseHandler):
    def get(self):
        print Bollocks.get_all()
        return self.render('index.slim', bollockses=Bollocks.get_all())


@app.route('/add_bollocks', 'add_bollocks')
class AddBollocks(BaseHandler):
    def get(self):
        return self.render('add_bollocks.slim')

    def post(self):
        he_said = self.request.get('he_said')
        it_means = self.request.get('it_means')
        b = Bollocks(he_said, it_means)
        b.push()
        raise redirect(uri_for('index'))


@app.route('/bollocks/<bollocks_id>', 'view_bollocks')
class ViewBollocks(BaseHandler):
    def get(self, bollocks_id):
        b = Bollocks.get_by_id(bollocks_id)
        return self.render('view_bollocks.slim', bollocks=b)
