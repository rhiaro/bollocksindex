from bollocksindex import utils
from bollocksindex.application import application
from bollocksindex.pages import pages
from paste import httpserver
from paste.cascade import Cascade
from paste.urlparser import StaticURLParser

static = StaticURLParser("./")

app = Cascade([application, static])

if __name__ == "__main__":
    httpserver.serve(app, host="", port="8080")
